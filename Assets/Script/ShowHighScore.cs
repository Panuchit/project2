﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowHighScore : MonoBehaviour
{
    public Text HIGHSCORE;
    public int HighScore;
    void Start()
    {
        HighScore = PlayerPrefs.GetInt("highScore", HighScore);
        HIGHSCORE.text = "HIGHSCORE: " + HighScore;


    }
 
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class controller : MonoBehaviour
{
    [SerializeField] float addForce;
    [SerializeField] float addTorque;
    

    
    Rigidbody rigibody;
    // Start is called before the first frame update
    void Start()
    {
        rigibody = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        Controller();

    }
    private void Controller()
    {

        if (Input.GetKey(KeyCode.W))
         {
             rigibody.AddForce(0, 0, addForce * Time.deltaTime);
             rigibody.AddTorque(addTorque * Time.deltaTime, 0, 0);


         }
         if (Input.GetKey(KeyCode.D))
         {

             rigibody.AddTorque(0, 0, (-addTorque) * Time.deltaTime);
             rigibody.AddForce(addForce * Time.deltaTime, 0, 0);
         }
         if (Input.GetKey(KeyCode.A))
         {

             rigibody.AddTorque(0, 0, addTorque * Time.deltaTime);
             rigibody.AddForce((-addForce) * Time.deltaTime, 0, 0);
         }
         if (Input.GetKey(KeyCode.S))
         {

             rigibody.AddForce((-addForce) * Time.deltaTime, 0, 0);
             rigibody.AddTorque((-addTorque) * Time.deltaTime, 0, 0);
         }

        
        

    }
    
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Obstacle"))
        {
            GetComponent<AudioSource>(); 
            SceneManager.LoadScene("End_sence");
            
        }
    }

}

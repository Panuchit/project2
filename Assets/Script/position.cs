﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class position : MonoBehaviour
{
    public GameObject playerPosition;
    Vector3 positionCamera;
    // Start is called before the first frame update
    void Start()
    {
        positionCamera = transform.position - playerPosition.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = playerPosition.transform.position + positionCamera;
    }
}

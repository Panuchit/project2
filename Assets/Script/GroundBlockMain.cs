﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundBlockMain : MonoBehaviour
{
    public float halfLength = 100f;
    public float endOffset = 10f;
    public Transform otherBlock;//ถนนใหม่
    public Transform player;
    void Start()
    {   
        
         player = GameObject.FindGameObjectWithTag("Player").transform;
    }
       void Update()
        {
            MoveGround();   
        }
       void MoveGround()
        {
            if (transform.position.z + halfLength < player.transform.position.z - endOffset)
            {
                transform.position = new Vector3(otherBlock.position.x, otherBlock.position.y, otherBlock.position.z + halfLength * 2);
            }
        }
    
}

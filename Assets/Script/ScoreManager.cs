﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class ScoreManager : MonoBehaviour
{
    public static int HighScore;
    public static int Score;
    public Text ScoreText;

    void Start()
    {
        Score = 0;


    }

    // Update is called once per frame
    void Update()
    {
        if (Score < 1)
        {
            ScoreText.text = "Score = 0";
        }
        if (Score > HighScore)
        {
            HighScore = Score;
            PlayerPrefs.SetInt("highScore", HighScore);
        }

    }
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Coin")
        {
            Score++;
            ScoreText.text = "Score = " + Score;
            GetComponent<AudioSource>();
            Destroy(other.gameObject);
            
            
        }

        if (Score == 20)
        {
            SceneManager.LoadScene("Game_Level2");

        }
    }
}
                
            

    
